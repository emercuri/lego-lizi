//-----------------------------------------------------------------------------
// Liz Mercuri - Lego Constructions

#define D3D_DEBUG_INFO

//-----------------------------------------------------------------------------
// Include these files
#include <Windows.h>	// Windows library (for window functions, menus, dialog boxes, etc)
#include <d3dx9.h>		// Direct 3D library (for all Direct 3D funtions).
#include <iostream>		//To use input and output file streams
#include <fstream>		//To be able to declare a file variable
#include <string>		//Store variables from the file as a string
#include <vector>
#include "Brick.h"

using namespace std;
//-----------------------------------------------------------------------------
// Global variables

LPDIRECT3D9             g_pD3D = NULL; // Used to create the D3DDevice
LPDIRECT3DDEVICE9       g_pd3dDevice = NULL; // The rendering device


LPDIRECT3DVERTEXBUFFER9 g_pBrickVB = NULL;

//For those pesky nodules - Haodi has helped me a lot with the maths for creating the cylinder structure
//However, I still feel like I need to revisit this
LPDIRECT3DVERTEXBUFFER9 g_pNoduleBaseVB = NULL; // Buffer to hold vertices for the triangles making up the nodule base
LPDIRECT3DVERTEXBUFFER9 g_pNoduleBase2VB = NULL; //  Buffer to hold vertices for the triangles making up the nodule base
LPDIRECT3DVERTEXBUFFER9 g_pNoduleTopVB = NULL; // Buffer to hold vertices for the circular top of the nodules
LPDIRECT3DVERTEXBUFFER9 g_pNoduleTop2VB = NULL; // Buffer to hold vertices for the circular top of the nodules

std::vector<Brick> Bricks;

//To create GroundPlane
Brick Ground;

// A structure for our custom vertex type
struct CUSTOMVERTEX
{
	D3DXVECTOR3 position;
	D3DXVECTOR3 normal;
	DWORD col;

};

// The structure of a vertex in our vertex buffer including normals and colour
#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE)
DWORD Colour;

//So a new vertex buffer isn't created at every createnodule() call - caused application to run out of memory - Thanks Jake!
HRESULT AllocateVertexBuffer();
//-----------------------------------------------------------------------------
// Global variables
int g_FloorVertices; //Used in render to determne number of triangles per structure
int g_ObjSize; //Used in render to determne number of triangles per structure
int g_ObjSizeCastle, g_ObjSizeCastleFront, g_ObjSizeBrick;

//For camera rotation
float g_CameraZ = 0.0f;
float g_CameraX = 0.0f;
float g_CameraY = 0.0f;

//Help from http://www.gamedev.net/topic/443548-rotate-camera-around-point/
float RadiusCam = 120.0f;
float Angle180 = (120.0f * D3DX_PI) / 2.0f;
float Angle90 = D3DX_PI / 2;

//-----------------------------------------------------------------------------
// Initialise Direct 3D.
// Requires a handle to the window in which the graphics will be drawn.
HRESULT SetupD3D(HWND hWnd)
{
	// Create the D3D object.
	if (NULL == (g_pD3D = Direct3DCreate9(D3D_SDK_VERSION)))
		return E_FAIL;

	// Set up the structure used to create the D3DDevice
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.Windowed = TRUE;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;

	// Create the D3DDevice
	if (FAILED(g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_SOFTWARE_VERTEXPROCESSING,
		&d3dpp, &g_pd3dDevice)))
	{
		return E_FAIL;
	}

	// Enable the Z buffer, since we're dealing with 3D geometry.
	g_pd3dDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);

	// Turn on culling.
	g_pd3dDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);

	g_pd3dDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	AllocateVertexBuffer();

	return S_OK;
}

//-----------------------------------------------------------------------------
// Release (delete) all the resources used by this program.
// Only release things if they are valid (i.e. have a valid pointer).
// If not, the program will crash at this point.
void CleanUp()
{
	if (g_pBrickVB != NULL)		g_pBrickVB->Release();



	if (g_pNoduleBaseVB != NULL)	g_pNoduleBaseVB->Release();
	if (g_pNoduleBase2VB != NULL)	g_pNoduleBase2VB->Release();
	if (g_pNoduleTopVB != NULL)		g_pNoduleTopVB->Release();
	if (g_pNoduleTop2VB != NULL)	g_pNoduleTop2VB->Release();

	if (g_pd3dDevice != NULL)	g_pd3dDevice->Release();
	if (g_pD3D != NULL)			g_pD3D->Release();
}


HRESULT AllocateVertexBuffer()
{
	//Nodule bases
	int Vertices = 10 * 2 * 3;	// Ten sides/segments, 2 triangles per segment, 3 vertices per triangle - times the number of nodules
	int BufferSize = Vertices * sizeof(CUSTOMVERTEX);

	if (FAILED(g_pd3dDevice->CreateVertexBuffer(BufferSize, 0, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT, &g_pNoduleBaseVB, NULL)))
	{
		return E_FAIL; // if the vertex buffer culd not be created.
	}

	int Vertices4 = 10 * 2 * 3;	// Ten sides/segments, 2 triangles per segment, 3 vertices per triangle - times the number of nodules
	int BufferSize4 = Vertices4 * sizeof(CUSTOMVERTEX);

	if (FAILED(g_pd3dDevice->CreateVertexBuffer(BufferSize4, 0, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT, &g_pNoduleBase2VB, NULL)))
	{
		return E_FAIL; // if the vertex buffer culd not be created.
	}

	//Nodule tops
	int BufferSize2 = (18 + 2) * sizeof(CUSTOMVERTEX); //18 sides

	if (FAILED(g_pd3dDevice->CreateVertexBuffer(BufferSize2, 0, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT, &g_pNoduleTopVB, NULL)))
	{
		return E_FAIL; // if the vertex buffer culd not be created.
	}

	int BufferSize3 = (18 + 2) * sizeof(CUSTOMVERTEX); //18 sides

	if (FAILED(g_pd3dDevice->CreateVertexBuffer(BufferSize3, 0, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT, &g_pNoduleTop2VB, NULL)))
	{
		return E_FAIL; // if the vertex buffer culd not be created.
	}


	int VerticesBrick = 36;
	int BufferSizeBrick = VerticesBrick * sizeof(CUSTOMVERTEX);

	if (FAILED(g_pd3dDevice->CreateVertexBuffer(BufferSizeBrick, 0, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT, &g_pBrickVB, NULL)))
	{
		return E_FAIL; // if the vertex buffer could not be created.
	}

	return S_OK;


}
//-----------------------------------------------------------------------------
// Set up the scene geometry.
HRESULT SetupGeometry()
{
	void SetupVertexData(CUSTOMVERTEX* pV, int index,
		float px, float py, float pz,
		float nx, float ny, float nz, DWORD col);

	HRESULT CreateBrickVB();

	//Create the GroundPlane
	D3DXMATRIX Wground, Tground, Sground;
	D3DXMatrixIdentity(&Wground);

	D3DXMatrixTranslation(&Tground, -2.0, -4.0, -2.0);
	D3DXMatrixScaling(&Sground, 16.0f, 0.5f, 16.0f);

	D3DXMatrixMultiply(&Wground, &Tground, &Sground);
	Ground.SetWorldMatrix(Wground);

	CreateBrickVB();

	D3DXMATRIX W;
	D3DXMatrixIdentity(&W);
	D3DXQUATERNION Rotation, ScalingRotation;
	D3DXVECTOR3 ScalingCentre(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 RotationCentre(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 Translate(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 Scaling(0.0f, 1.0f, 1.0f);
	D3DXQuaternionRotationYawPitchRoll(&Rotation, 0.0f, 0.0f, D3DXToRadian(0.0));
	D3DXQuaternionRotationYawPitchRoll(&ScalingRotation, 0.0f, 0.0f, 0.0f);



	ifstream inputfile("data.txt");


	if (inputfile.is_open())
	{
		int numBricks = 0;
		inputfile >> numBricks;

		Bricks.resize(numBricks);

		for (int i = 0; i < numBricks; ++i)
		{
			int type = 0;
			inputfile >> type;

			//For input file
			float x = 0.0f, y = 0.0f, z = 0.0f;
			int r = 0, g = 0, b = 0;

			inputfile >> x >> y >> z >> r >> g >> b;
			//Get Brick Y position
			Bricks[i].SetBrickPosY(y);

			Translate = D3DXVECTOR3(x, y, z);

			//For Colour
			Bricks[i].SetBrickColour(D3DCOLOR_RGBA(r, g, b, 0));

			//inputfile >> r >> g >> b;


			//For scaling 
			Bricks[i].SetBrickType((BrickType)type);



			switch ((BrickType)type)
			{

			case TwoByTwo:
				Scaling = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
				break;

			case TwoByFour:
				Scaling = D3DXVECTOR3(2.0f, 1.0f, 1.0f);
				break;

			case TwoByEight:
				Scaling = D3DXVECTOR3(4.0f, 1.0f, 1.0f);
				break;

			default:
				Scaling = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
				break;
			}


			D3DXMatrixTransformation(&W, &ScalingCentre, &ScalingRotation, &Scaling, &RotationCentre, &Rotation, &Translate);
			Bricks[i].SetWorldMatrix(W);




		}


		CreateBrickVB();


		inputfile.close();
	}

	else
	{
		cout << "There is an error";
	}





	return S_OK;
}

// Set up a vertex data.
void SetupVertexData(CUSTOMVERTEX* pV, int index,
	float px, float py, float pz,
	float nx, float ny, float nz, DWORD col)
{
	pV[index].position.x = px;	// Vertex co-ordinate
	pV[index].position.y = py;
	pV[index].position.z = pz;
	pV[index].normal.x = nx;	// Vertex normal - lighting
	pV[index].normal.y = ny;
	pV[index].normal.z = nz;
	pV[index].col = col;

}


HRESULT CreateBrickVB()
{

	//DWORD colour = D3DCOLOR_RGBA(r, g, b, 0);

	CUSTOMVERTEX *pVerticesBrick;

	if (FAILED(g_pBrickVB->Lock(0, 0, (void**)&pVerticesBrick, 0)))
	{
		return E_FAIL;  // if the pointer to the vertex buffer could not be established.
	}

	//float val = 4.0f;
	for (int i = 0; i < Bricks.size(); ++i)
	{


		SetupVertexData(pVerticesBrick, 0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 1, 0.0, 4.0, 0.0, 0.0, 0.0, -1.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 2, 4.0, 0.0, 0.0, 0.0, 0.0, -1.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 3, 4.0, 0.0, 0.0, 0.0, 0.0, -1.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 4, 0.0, 4.0, 0.0, 0.0, 0.0, -1.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 5, 4.0, 4.0, 0.0, 0.0, 0.0, -1.0, Bricks[i].GetBrickColour());
		// Side 2 - Right face
		SetupVertexData(pVerticesBrick, 6, 4.0, 0.0, 0.0, 1.0, 0.0, 0.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 7, 4.0, 4.0, 0.0, 1.0, 0.0, 0.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 8, 4.0, 0.0, 4.0, 1.0, 0.0, 0.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 9, 4.0, 0.0, 4.0, 1.0, 0.0, 0.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 10, 4.0, 4.0, 0.0, 1.0, 0.0, 0.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 11, 4.0, 4.0, 4.0, 1.0, 0.0, 0.0, Bricks[i].GetBrickColour());
		//// Side 3 - Rear face
		SetupVertexData(pVerticesBrick, 12, 4.0, 0.0, 4.0, 0.0, 0.0, 1.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 13, 4.0, 4.0, 4.0, 0.0, 0.0, 1.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 14, 0.0, 0.0, 4.0, 0.0, 0.0, 1.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 15, 0.0, 0.0, 4.0, 0.0, 0.0, 1.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 16, 4.0, 4.0, 4.0, 0.0, 0.0, 1.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 17, 0.0, 4.0, 4.0, 0.0, 0.0, 1.0, Bricks[i].GetBrickColour());
		//// Side 4 - Left face
		SetupVertexData(pVerticesBrick, 18, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 19, 0.0, 4.0, 4.0, -1.0, 0.0, 0.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 20, 0.0, 4.0, 0.0, -1.0, 0.0, 0.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 21, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 22, 0.0, 0.0, 4.0, -1.0, 0.0, 0.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 23, 0.0, 4.0, 4.0, -1.0, 0.0, 0.0, Bricks[i].GetBrickColour());
		//// Side 5 - Top face
		SetupVertexData(pVerticesBrick, 24, 0.0, 4.0, 0.0, 0.0, 1.0, 0.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 25, 0.0, 4.0, 4.0, 0.0, 1.0, 0.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 26, 4.0, 4.0, 0.0, 0.0, 1.0, 0.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 27, 4.0, 4.0, 0.0, 0.0, 1.0, 0.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 28, 0.0, 4.0, 4.0, 0.0, 1.0, 0.0, Bricks[i].GetBrickColour());
		SetupVertexData(pVerticesBrick, 29, 4.0, 4.0, 4.0, 0.0, 1.0, 0.0, Bricks[i].GetBrickColour());
	}


	g_pBrickVB->Unlock();


	return S_OK;
}


#pragma endregion

//-----------------------------------------------------------------------------
// Set up the view - the view and projection matrices.
void SetupViewMatrices()
{
	//Angles set for camera rotation each render
	//g_CameraX = RadiusCam * cos(Angle180) * sin(Angle90);
	//g_CameraY = 100.0f;
	//g_CameraZ = RadiusCam * sin(Angle180) * sin(Angle90);

	//For static viewing
	g_CameraX = 50.0f;
	g_CameraY = 60.0f;
	g_CameraZ = -140.0f;

	D3DXVECTOR3 vCamera(g_CameraX, g_CameraY, g_CameraZ);
	D3DXVECTOR3 vLookat(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 vUpVector(0.0f, 1.0f, 0.0f);
	D3DXMATRIX matView;
	D3DXMatrixLookAtLH(&matView, &vCamera, &vLookat, &vUpVector);
	g_pd3dDevice->SetTransform(D3DTS_VIEW, &matView);

	// Set up the projection matrix.
	D3DXMATRIX matProj;
	D3DXMatrixPerspectiveFovLH(&matProj, D3DX_PI / 4, 1.0f, 1.0f, 500.0f);
	g_pd3dDevice->SetTransform(D3DTS_PROJECTION, &matProj);
}

//---------------------------------------------------------------------------------
void SetupPointLight()
{
	// Define a light - setting the diffuse colour only.
	D3DLIGHT9 MainLight;
	ZeroMemory(&MainLight, sizeof(D3DLIGHT9));
	MainLight.Type = D3DLIGHT_POINT;

	MainLight.Diffuse.r = 1.0f;
	MainLight.Diffuse.g = 1.0f;
	MainLight.Diffuse.b = 1.0f;

	MainLight.Specular.r = 1.0f;
	MainLight.Specular.g = 1.0f;
	MainLight.Specular.b = 1.0f;

	MainLight.Position.x = 100.0f;
	MainLight.Position.y = 50.0f;
	MainLight.Position.z = -150.0f;

	MainLight.Attenuation0 = 1.0f;
	MainLight.Attenuation1 = 0.0f;
	MainLight.Attenuation2 = 0.0f;

	MainLight.Range = 300.0f;

	// Select and enable the light.
	g_pd3dDevice->SetLight(0, &MainLight);
	g_pd3dDevice->LightEnable(0, TRUE);
}

// Render the scene.
void Render()
{
	// Clear the backbuffer to a blue colour, also clear the Z buffer at the same time.
	g_pd3dDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(50, 50, 150), 1.0f, 0);

	// Begin the scene
	if (SUCCEEDED(g_pd3dDevice->BeginScene()))
	{
		SetupViewMatrices();


		D3DXMATRIX TranslateMat, ScaleMat, IdentMat;
		D3DXMatrixIdentity(&IdentMat);

		//Render block
		g_pd3dDevice->SetFVF(D3DFVF_CUSTOMVERTEX);
		g_pd3dDevice->SetStreamSource(0, g_pBrickVB, 0, sizeof(CUSTOMVERTEX));

		for (int i = 0; i < Bricks.size(); ++i)
		{

		//Trying to create a falling block
		//float BrickPosY = Bricks[i].GetBrickPosY();
		//float StartPosY = 20.0f;

		//// Construct a translation matrix to move the rectangle.
		//D3DXMATRIX TranslateMat;
		//D3DXMatrixTranslation(&TranslateMat, 0.0f, StartPosY, 0.0f);


		// Update the rectangle's x co-ordinate.
		/*if (StartPosY > BrickPosY)
		{
		StartPosY -= 5.0f;
		}*/

		//g_pd3dDevice->SetTransform(D3DTS_WORLD, &TranslateMat);

		g_pd3dDevice->SetTransform(D3DTS_WORLD, &Bricks[i].GetWorldMatrix());
		g_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 10);

		}

		g_pd3dDevice->SetTransform(D3DTS_WORLD, &Ground.GetWorldMatrix());
		g_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 10);

		// End the scene.
		g_pd3dDevice->EndScene();
	}

	g_pd3dDevice->Present(NULL, NULL, NULL, NULL);
}

//-----------------------------------------------------------------------------
// The window's message handling function.
LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		return 0;
	}


	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}

//-----------------------------------------------------------------------------
// WinMain() - The application's entry point.
int WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR, int)
{
	// Register the window class
	WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
		GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
		"Liz Lego", NULL };
	RegisterClassEx(&wc);

	// Create the application's window
	HWND hWnd = CreateWindow("Liz Lego", "Liz Mercuri Lego",
		WS_OVERLAPPEDWINDOW, 100, 100, 600, 600,
		GetDesktopWindow(), NULL, wc.hInstance, NULL);

	// Initialise Direct3D
	if (SUCCEEDED(SetupD3D(hWnd)))
	{
		// Create the scene geometry
		if (SUCCEEDED(SetupGeometry()))
		{
			// Show the window
			ShowWindow(hWnd, SW_SHOWDEFAULT);
			UpdateWindow(hWnd);

			// Set up the light.
			SetupPointLight();


			// Define the viewpoint.
			SetupViewMatrices();

			// Enter the message loop
			MSG msg;
			ZeroMemory(&msg, sizeof(msg));
			while (msg.message != WM_QUIT)
			{
				if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
				else
					Angle180 += 0.01f;
				Render();
			}
		}
		CleanUp();
	}

	UnregisterClass("Liz Lego", wc.hInstance);
	return 0;
}
