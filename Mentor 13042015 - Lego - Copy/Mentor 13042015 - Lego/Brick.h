#pragma once
#include <d3dx9.h>	

enum BrickType
{
	TwoByTwo,
	TwoByFour,
	TwoByEight,

	NumBrickTypes
};

class Brick
{
public:
	Brick();
	~Brick();

	void SetWorldMatrix(const D3DXMATRIX &wm);
	const D3DXMATRIX& GetWorldMatrix() const;

	void SetBrickType(BrickType bt);
	void SetBrickColour(DWORD Colour);
	void SetBrickPosY(float pos);

	BrickType GetBrickType() const;
	DWORD GetBrickColour() const;
	float GetBrickPosY() const;

private:
	D3DXMATRIX _WorldMatrix;
	BrickType _BrickType;
	DWORD _BrickColour;
	float _BrickPosY;
};

