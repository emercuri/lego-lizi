#include "Brick.h"


Brick::Brick()
{
	D3DXMatrixIdentity(&_WorldMatrix);
}


Brick::~Brick()
{
}

void Brick::SetWorldMatrix(const D3DXMATRIX &wm)
{
	_WorldMatrix = wm;
}

const D3DXMATRIX& Brick::GetWorldMatrix() const
{
	return _WorldMatrix;
}

void Brick::SetBrickType(BrickType bt)
{
	_BrickType = bt;
}

BrickType Brick::GetBrickType() const
{
	return _BrickType;
}

void Brick::SetBrickColour(DWORD col)
{
	_BrickColour = col;
}

DWORD Brick::GetBrickColour() const
{
	return _BrickColour;
}

void Brick::SetBrickPosY(float pos)
{
	
	_BrickPosY = pos;
}

float Brick::GetBrickPosY() const
{
	return _BrickPosY;
}